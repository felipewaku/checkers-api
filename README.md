# Checkers API

## Description

This project is an implementation of a REST Checkers API using Node, express and mongo using [danielfsouza's boilerplate](https://github.com/danielfsousa/express-rest-es2017-boilerplate) as starting point.

## Prerequisites

* nvm
* yarn
* Docker environment

## Project Setup

1. Ensure the correct Nove version by using `$ nvm use`
2. Install npm dependencies using yarn `$ yarn`
3. Start the mongo's docker image:
  - `$ docker-compose up -d`
4. Create a `.env` basead on .env.sample
  - `$ cp .env.sample .env`

## Running tests

```bash
$ yarn test
```

## Running project locally

```bash
$ yarn dev
```

## Running project in production

```bash
$ yarn start
```

## Project structure

Inside the directory `postman` there is a simple collection to test all endpoints and a environment to test the app locally.

In `src` can be found the project's source code.

- `config`: Contains the code to setup the express server, setup passport authentication, setup mongoose and load environment variables.
- `api`: Contains the api's code.
  - `routes`: Express' routes
  - `middlewares`: Express' middlewares
  - `models`: Mongoose's models
  - `controllers`: Controllers to connect routes with model's functions
  - `tests`: Automated tests
  - `services`: Functions to compute possible moves of a piece in a board
  - `utils`: APIError
  