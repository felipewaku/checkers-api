
const Piece = {
  menBlack: 'b',
  menWhite: 'w',
  kingBlack: 'B',
  kingWhite: 'W',
};

const GameState = {
  waitingOpponent: 'waiting for opponent',
  playerOneTurn: 'player_1 turn',
  playerTwoTurn: 'player_2 turn',
  playerOneWon: 'player_1 won',
  playerTwoWon: 'player_2 won',
  draw: 'draw',
};

const PlayerTurn = {
  playerOne: 'player_1',
  playerTwo: 'player_2',
};

exports.Piece = Piece;
exports.PlayerTurn = PlayerTurn;
exports.GameState = GameState;
