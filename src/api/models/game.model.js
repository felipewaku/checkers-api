const httpStatus = require('http-status');
const mongoose = require('mongoose');
const jwt = require('jwt-simple');
const { jwtSecret } = require('../../config/vars');
const APIError = require('../utils/APIError');
const getMovesOfPiece = require('../services/piece-moves.service');
const { Piece, GameState, PlayerTurn } = require('./game.entities');

const INITIAL_BOARD = [
  [null, Piece.menBlack, null, Piece.menBlack, null, Piece.menBlack, null, Piece.menBlack],
  [Piece.menBlack, null, Piece.menBlack, null, Piece.menBlack, null, Piece.menBlack, null],
  [null, Piece.menBlack, null, Piece.menBlack, null, Piece.menBlack, null, Piece.menBlack],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [Piece.menWhite, null, Piece.menWhite, null, Piece.menWhite, null, Piece.menWhite, null],
  [null, Piece.menWhite, null, Piece.menWhite, null, Piece.menWhite, null, Piece.menWhite],
  [Piece.menWhite, null, Piece.menWhite, null, Piece.menWhite, null, Piece.menWhite, null],
];

/**
 * Game Schema
 * @private
 */
const gameSchema = new mongoose.Schema(
  {
    board: {
      type: [[String]],
      required: true,
      default() { return INITIAL_BOARD; },
    },
    playerOne: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      default() { return new mongoose.Types.ObjectId(); },
    },
    playerTwo: {
      type: mongoose.Schema.Types.ObjectId,
    },
    state: {
      type: String,
      require: true,
      default() { return GameState.waitingOpponent; },
    },
  },
  {
    timestamps: true,
  },
);

gameSchema.statics.get = async function get(gameId) {
  try {
    let game;

    if (mongoose.Types.ObjectId.isValid(gameId)) {
      game = await this.findById(gameId).exec();
    }
    if (game) {
      return game;
    }

    throw new APIError({
      message: 'Game does not exist.',
      status: httpStatus.NOT_FOUND,
    });
  } catch (error) {
    throw error;
  }
};

gameSchema.statics.createGame = async function createGame() {
  const game = new Game();
  const { id: gameId, playerOne } = await game.save();
  const playerOneToken = jwt.encode({ gameId, playerOne }, jwtSecret);
  return { gameId, playerOneToken };
};

gameSchema.methods.setupPlayerTwo = async function setupPlayerTwo() {
  if (this.playerTwo !== undefined) {
    throw new APIError({
      message: 'Game has already a Player Two.',
      status: httpStatus.INTERNAL_SERVER_ERROR,
    });
  }
  this.playerTwo = new mongoose.Types.ObjectId();
  this.state = GameState.playerOneTurn;
  const { id: gameId, playerTwo } = await this.save();
  const playerTwoToken = jwt.encode({ gameId, playerTwo }, jwtSecret);
  return { gameId, playerTwoToken };
};

gameSchema.methods.checkPlayerWin = function checkPlayerWin(requestPlayer) {
  const enemyMen = requestPlayer === PlayerTurn.playerOne ? Piece.menBlack : Piece.menWhite;
  const enemyKing = requestPlayer === PlayerTurn.playerOne ? Piece.kingBlack : Piece.kingWhite;
  return !this.board.reduce(
    (hasEnemies, row) =>
      hasEnemies || row.reduce((hasEnemyInRow, tile) => hasEnemyInRow || tile === enemyKing || tile === enemyMen, false),
    false,
  );
};

gameSchema.methods.getState = async function getState() {
  const { board, state } = this;
  return { board, state };
};

gameSchema.methods.getMoves = async function getMoves(row, column, requestPlayer) {
  const { board } = this;
  const moves = getMovesOfPiece(board, row, column, requestPlayer);
  return { moves };
};

gameSchema.methods.getBoardForMove = function getBoardForMove(row, column, move, requestPlayer) {
  const newBoard = this.board.map(line => line.slice());
  const piece = newBoard[row][column];
  newBoard[row][column] = null;
  const { destination, captures } = move;
  const [newRow, newColumn] = destination;

  captures.forEach(([captureRow, captureColumn]) => {
    newBoard[captureRow][captureColumn] = null;
  });

  newBoard[newRow][newColumn] = piece;

  const lastRow = requestPlayer === PlayerTurn.playerOne ? 0 : newBoard.length - 1;
  const isMen = piece === Piece.menBlack || piece === Piece.menWhite;
  if (newRow === lastRow && isMen) {
    const king = requestPlayer === PlayerTurn.playerOne ? Piece.kingWhite : Piece.kingBlack;
    newBoard[newRow][newColumn] = king;
  }
  return newBoard;
};

function areMovesEqual(firstMove, secondMove) {
  const { destination: firstDestination, captures: firstCatpures } = firstMove;
  const { destination: secondDestination, captures: secondCatpures } = secondMove;

  const areBothDestinationsEqual =
    firstDestination[0] === secondDestination[0] && firstDestination[1] === secondDestination[1];

  if (!areBothDestinationsEqual) {
    return false;
  }

  const sameNumberOfCaptures = firstCatpures.length === secondCatpures.length;

  if (!sameNumberOfCaptures) {
    return false;
  }

  const bothHaveTheSameCaptures =
    firstCatpures.reduce((areEqual, capture, index) =>
      areEqual && capture[0] === secondCatpures[index][0] && capture[1] === secondCatpures[index][1], true);

  return bothHaveTheSameCaptures;
}

gameSchema.methods.movePiece = async function movePiece(row, column, move, requestPlayer) {
  const isPlayerTurn = (requestPlayer === PlayerTurn.playerOne && this.state === GameState.playerOneTurn) ||
    (requestPlayer === PlayerTurn.playerTwo && this.state === GameState.playerTwoTurn);
  if (!isPlayerTurn) {
    throw new APIError({
      message: 'Not player turn.',
      status: httpStatus.INTERNAL_SERVER_ERROR,
    });
  }

  const { board } = this;
  const validMoves = getMovesOfPiece(board, row, column, requestPlayer);
  const isValidMoveForPiece = validMoves.find(validMove => areMovesEqual(validMove, move)) !== undefined;
  if (!isValidMoveForPiece) {
    throw new APIError({
      message: 'Invalid move.',
      status: httpStatus.BAD_REQUEST,
    });
  }

  if (move.captures.length === 0) {
    const piecesWithCaptures = [];
    board.forEach((row, rowIndex) => row.forEach((tile, columnIndex) => {
      const playerPieces = 
        requestPlayer === PlayerTurn.playerOne ? [Piece.menWhite, Piece.kingWhite] : [Piece.menBlack, Piece.kingBlack]
      const isPieceFromPlayer = playerPieces.includes(tile);
      if (isPieceFromPlayer) {
        const movesOfTestPiece = getMovesOfPiece(board, rowIndex, columnIndex, requestPlayer);
        movesOfTestPiece.forEach(moveOfTestPiece => {
          if (moveOfTestPiece.captures.length > 0) {
            piecesWithCaptures.push([rowIndex, columnIndex]);
          }
        });
      }
    }));

    if (piecesWithCaptures.length > 0) {
      throw new APIError({
        message: `There are captures available for pieces: ${pieces.map(piece => `[${piece.join(', ')}]`).join(', ') }`,
        status: httpStatus.INTERNAL_SERVER_ERROR,
      });
    }
  }

  this.board = this.getBoardForMove(row, column, move, requestPlayer);

  const didRequestPlayerWon = this.checkPlayerWin(requestPlayer);
  if (didRequestPlayerWon) {
    this.state = requestPlayer === PlayerTurn.playerOne ? GameState.playerOneWon : GameState.playerTwoWon;
  } else {
    this.state = requestPlayer === PlayerTurn.playerOne ? GameState.playerTwoTurn : GameState.playerOneTurn;
  }
  return this.save();
};

/**
 * @typedef Game
 */
const Game = mongoose.model('Game', gameSchema);
exports.Game = Game;
