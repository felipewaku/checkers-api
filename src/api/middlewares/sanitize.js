const httpStatus = require('http-status');
const APIError = require('../utils/APIError');

exports.sanitize = (req, res, next) => {
  const { params, tokenData } = req;
  const { game } = tokenData;
  const { row: rawRow, column: rawColumn } = params;

  const row = Number(rawRow);
  const column = Number(rawColumn);
  if (Number.isNaN(row) || Number.isNaN(column)) {
    return next(new APIError({
      message: 'Invalid row or column sent.',
      status: httpStatus.BAD_REQUEST,
    }));
  }

  const maxRow = game.board.length - 1;
  const maxColumn = game.board[0].length - 1;

  if (row > maxRow || column > maxColumn) {
    return next(new APIError({
      message: 'Row or column sent outside borders',
      status: httpStatus.BAD_REQUEST,
    }));
  }

  req.position = [row, column];

  return next();
};

