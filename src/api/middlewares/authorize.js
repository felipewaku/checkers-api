const httpStatus = require('http-status');
const passport = require('passport');
const APIError = require('../utils/APIError');

const handleJWT = (req, res, next) => async (err, tokenData) => {
  if (err || !tokenData) {
    return next(new APIError({
      message: err ? err.message : 'Unauthorized',
      status: httpStatus.UNAUTHORIZED,
      stack: err ? err.stack : undefined,
    }));
  }

  req.tokenData = tokenData;

  return next();
};

exports.authorize = (req, res, next) =>
  passport.authenticate(
    'gameJwt', { session: false },
    handleJWT(req, res, next),
  )(req, res, next);

