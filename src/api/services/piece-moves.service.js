const httpStatus = require('http-status');
const { Piece, PlayerTurn } = require('../models/game.entities');
const APIError = require('../utils/APIError');

const DIRECTION_LEFT = -1;
const DIRECTION_RIGHT = 1;

const DIRECTION_UP = -1;
const DIRECTION_DOWN = 1;

const ROW = 0;
const COLUMN = 1;

function isPieceFromPlayer(piece, player) {
  if (player === PlayerTurn.playerOne) {
    return piece === Piece.menWhite || piece === Piece.kingWhite;
  }

  return piece === Piece.menBlack || piece === Piece.kingBlack;
}

function isKing(piece) {
  const kings = [Piece.kingBlack, Piece.kingWhite];
  return kings.includes(piece);
}


function isEnemyPiece(piece, player) {
  if (piece === null || piece === undefined) {
    return false;
  }

  return !isPieceFromPlayer(piece, player);
}

function getNextMoves(board, currentPosition, player) {
  const [row, column] = currentPosition;

  function handleCapture(position, captures, verticalDirection) {
    function handleMultipleCapture(direction, vDirection) {
      const testTileRowIndex = position[ROW] + vDirection;
      function canCapture() {
        const testTileColumnIndex = position[COLUMN] + direction;

        const movementRow = board[testTileRowIndex];
        if (movementRow === undefined) {
          return false;
        }
        const testTile = movementRow[testTileColumnIndex];

        if (captures.findIndex(([rowCapture, columnCapture]) => rowCapture === testTileRowIndex && columnCapture === testTileColumnIndex) >= 0) {
          return false;
        }

        if (isEnemyPiece(testTile, player)) {
          const captureDestinationRowIndex = testTileRowIndex + vDirection;
          const captureDestinationColumnIndex = testTileColumnIndex + direction;
          const isCaptureDestinationEmpty = board[captureDestinationRowIndex][captureDestinationColumnIndex] === null;
          const isOriginalPosition = captureDestinationRowIndex === row && captureDestinationColumnIndex === column;

          return isCaptureDestinationEmpty || isOriginalPosition;
        }
        return false;
      }

      if (canCapture()) {
        const testTileColumnIndex = position[COLUMN] + direction;
        const captureDestinationRowIndex = testTileRowIndex + vDirection;
        const captureDestinationColumnIndex = testTileColumnIndex + direction;
        const newPosition = [captureDestinationRowIndex, captureDestinationColumnIndex];
        const newCapture = [testTileRowIndex, testTileColumnIndex];
        return handleCapture(newPosition, [newCapture, ...captures], vDirection);
      }

      return [];
    }

    const leftCapturesRegular = handleMultipleCapture(DIRECTION_LEFT, verticalDirection);
    const rightCapturesRegular = handleMultipleCapture(DIRECTION_RIGHT, verticalDirection);

    let leftCapturesKing = [];
    let rightCapturesKing = [];
    if (isKing(board[row][column])) {
      leftCapturesKing = handleMultipleCapture(DIRECTION_LEFT, -verticalDirection);
      rightCapturesKing = handleMultipleCapture(DIRECTION_RIGHT, -verticalDirection);
    }

    if (leftCapturesRegular.length === 0 && rightCapturesRegular.length === 0 && leftCapturesKing.length === 0 && rightCapturesKing.length === 0) {
      return [{ destination: position, captures }];
    }

    return leftCapturesRegular.concat(rightCapturesRegular).concat(leftCapturesKing).concat(rightCapturesKing);
  }

  function handleMovement(horizontalDirection, verticalDirection) {
    const testTileColumnIndex = column + horizontalDirection;
    const testTileRowIndex = row + verticalDirection;

    const movementRow = board[testTileRowIndex];
    const testTile = movementRow[testTileColumnIndex];
    const isMovePossible = testTile === null;

    if (isMovePossible) {
      return [{
        destination: [testTileRowIndex, testTileColumnIndex],
        captures: [],
      }];
    }

    const captureDestinationRowIndex = testTileRowIndex + verticalDirection;
    const captureDestinationColumnIndex = testTileColumnIndex + horizontalDirection;
    const isCaptureDestinationEmpty = board[captureDestinationRowIndex][captureDestinationColumnIndex] === null;

    if (isEnemyPiece(testTile, player) && isCaptureDestinationEmpty) {
      const captures = [[testTileRowIndex, testTileColumnIndex]];
      return handleCapture([captureDestinationRowIndex, captureDestinationColumnIndex], captures, verticalDirection);
    }

    return [];
  }

  const moves = [];
  const playerDirection = player === PlayerTurn.playerOne ? DIRECTION_UP : DIRECTION_DOWN;
  moves.push(...handleMovement(DIRECTION_LEFT, playerDirection));
  moves.push(...handleMovement(DIRECTION_RIGHT, playerDirection));

  if (isKing(board[row][column])) {
    moves.push(...handleMovement(DIRECTION_LEFT, -playerDirection));
    moves.push(...handleMovement(DIRECTION_RIGHT, -playerDirection));
  }

  return moves;
}

/**
 * @param {[String]} board The Game's current board
 * @param {Number} row The piece's row to check the moves
 * @param {Number} column The piece's column to check the moves
 * @param {PlayerTurn} player The player that is requesting the moves
 */
function getMovesOfPiece(board, row, column, player) {

  const piece = board[row][column];
  if (!piece) {
    throw new APIError({
      message: 'Piece not found.',
      status: httpStatus.NOT_FOUND,
    });
  }

  const isValidPiece = isPieceFromPlayer(piece, player);
  if (!isValidPiece) {
    throw new APIError({
      message: 'Piece doen\'t belong to the player.',
      status: httpStatus.INTERNAL_SERVER_ERROR,
    });
  }
  const moves = getNextMoves(board, [row, column], player);
  const captures = moves.filter(move => move.captures.length > 0);
  return captures.length > 0 ? captures : moves;
}

module.exports = getMovesOfPiece;
