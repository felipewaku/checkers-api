const httpStatus = require('http-status');
const { Game } = require('../models/game.model');
const { handler: errorHandler } = require('../middlewares/error');

/**
 * Load game and append to req.
 * @public
 */
exports.load = async (req, res, next, id) => {
  try {
    const game = await Game.get(id);
    req.locals = { game };
    return next();
  } catch (error) {
    return errorHandler(error, req, res);
  }
};


/**
 * Create a new game in the database and the return its gameId
 * and the player two's access token
 * @public
 */
exports.create = async (req, res, next) => {
  try {
    const { playerOneToken, gameId } = await Game.createGame();
    res.append('Authorization', `Bearer ${playerOneToken}`);
    res.status(httpStatus.CREATED);
    res.json({ gameId });
  } catch (err) {
    next(err);
  }
};

exports.registerPlayerTwo = async (req, res, next) => {
  try {
    const { game } = req.locals;
    const { playerTwoToken, gameId } = await game.setupPlayerTwo();
    res.append('Authorization', `Bearer ${playerTwoToken}`);
    res.json({ gameId });
  } catch (err) {
    next(err);
  }
};

exports.get = async (req, res, next) => {
  try {
    const { tokenData } = req;
    const { game } = tokenData;
    const state = await game.getState();
    res.json(state);
  } catch (err) {
    next(err);
  }
};


exports.getMoves = async (req, res, next) => {
  try {
    const { tokenData, position } = req;
    const { game, requestPlayer } = tokenData;
    const [row, column] = position;
    const moves = await game.getMoves(row, column, requestPlayer);
    res.json(moves);
  } catch (err) {
    next(err);
  }
};

exports.move = async (req, res, next) => {
  try {
    const { position, tokenData, body } = req;
    const { game, requestPlayer } = tokenData;
    const [row, column] = position;
    await game.movePiece(row, column, body, requestPlayer);
    res.status(httpStatus.NO_CONTENT).send();
  } catch (err) {
    next(err);
  }
};

