/* eslint-disable arrow-body-style */
/* eslint-disable no-unused-expressions */
const request = require('supertest');
const httpStatus = require('http-status');
const { expect } = require('chai');
const app = require('../../../index');
const { Game } = require('../../models/game.model');
const { GameState } = require('../../models/game.entities');

describe('POST /v1/game/:gameId/register', async () => {
  let gameId;

  beforeEach(async () => {
    const { gameId: id } = await Game.createGame();
    gameId = id;
  });

  afterEach(async () => {
    await Game.remove({});
  });

  it('should register player two and change the game\'s state', async () => {
    const res = await request(app)
      .post(`/v1/game/${gameId}/register`)
      .expect(httpStatus.OK);

    expect(res.body.gameId).to.be.equal(gameId);
    const [game] = await Game.find();
    expect(game.state).to.be.equal(GameState.playerOneTurn);
  });

  it('should not register a second player two', async () => {
    const [game] = await Game.find();
    await game.setupPlayerTwo();

    await request(app)
      .post(`/v1/game/${gameId}/register`)
      .expect(httpStatus.INTERNAL_SERVER_ERROR);
  });
});
