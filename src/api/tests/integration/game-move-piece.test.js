/* eslint-disable arrow-body-style */
/* eslint-disable no-unused-expressions */
const request = require('supertest');
const httpStatus = require('http-status');
const { expect } = require('chai');
const app = require('../../../index');
const { Game } = require('../../models/game.model');
const { GameState, Piece } = require('../../models/game.entities');

const KING_BOARD_TEST = [
  [null, null, null, null, null, null, null, null],
  [null, Piece.menWhite, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, Piece.menBlack, null, null, null, null],
  [null, null, null, null, null, null, null, null],
];

const FORCE_CAPTURE_BOARD_TEST = [
  [null, null, null, null, null, null, null, null],
  [null, Piece.menBlack, null, null, null, null, null, null],
  [Piece.menWhite, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [Piece.menWhite, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
];

const FINISH_GAME_BOARD_TEST = [
  [null, null, null, null, null, null, null, null],
  [null, Piece.menBlack, null, null, null, null, null, null],
  [Piece.menWhite, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
];

const FINISHED_GAME_BOARD_TEST = [
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [Piece.menWhite, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
];

describe('POST /v1/game/move/:row/:column', async () => {
  let playerOneToken;
  let playerTwoToken;
  let gameId;

  beforeEach(async () => {
    const { gameId: id, playerOneToken: token } = await Game.createGame();
    playerOneToken = token;
    gameId = id;
    const game = await Game.findById(gameId);
    const { playerTwoToken: secondToken } = await game.setupPlayerTwo();
    playerTwoToken = secondToken;
  });

  afterEach(async () => {
    await Game.remove({});
  });

  it('should move a piece and pass the turn', async () => {
    const row = 5;
    const column = 0;
    const move = {
      destination: [4, 1],
      captures: [],
    };
    await request(app)
      .post(`/v1/game/move/${row}/${column}`)
      .set('Authorization', `Bearer  ${playerOneToken}`)
      .send(move)
      .expect(httpStatus.NO_CONTENT);
    const game = await Game.findById(gameId);
    expect(game.board[row][column]).to.be.equal(null);
    expect(game.board[move.destination[0]][move.destination[1]]).to.be.equal(Piece.menWhite);
    expect(game.state).to.be.equal(GameState.playerTwoTurn);
  });

  it('should throw an error if tries to move a piece when there is a possible capture', async () => {

    let game = await Game.findById(gameId);
    game.board = FORCE_CAPTURE_BOARD_TEST;
    await game.save();

    const row = 4;
    const column = 0;
    const move = {
      destination: [3, 1],
      captures: [],
    };
    await request(app)
      .post(`/v1/game/move/${row}/${column}`)
      .set('Authorization', `Bearer  ${playerTwoToken}`)
      .send(move)
      .expect(httpStatus.INTERNAL_SERVER_ERROR);
  });

  it('should not move during enemy\'s turn', async () => {
    const row = 2;
    const column = 1;
    const move = {
      destination: [3, 0],
      captures: [],
    };
    await request(app)
      .post(`/v1/game/move/${row}/${column}`)
      .set('Authorization', `Bearer  ${playerTwoToken}`)
      .send(move)
      .expect(httpStatus.INTERNAL_SERVER_ERROR);
  });

  it('should promote piece to king if necessary', async () => {
    const row = 1;
    const column = 1;
    const move = {
      destination: [0, 0],
      captures: [],
    };
    let game = await Game.findById(gameId);
    game.board = KING_BOARD_TEST;
    await game.save();
    await request(app)
      .post(`/v1/game/move/${row}/${column}`)
      .set('Authorization', `Bearer  ${playerOneToken}`)
      .send(move)
      .expect(httpStatus.NO_CONTENT);
    game = await Game.findById(gameId);
    expect(game.board[0][0]).to.be.equal(Piece.kingWhite);
  });

  it('should finish game if possible', async () => {
    const row = 2;
    const column = 0;
    const move = {
      destination: [0, 2],
      captures: [[1, 1]],
    };
    let game = await Game.findById(gameId);
    game.board = FINISH_GAME_BOARD_TEST;
    await game.save();
    await request(app)
      .post(`/v1/game/move/${row}/${column}`)
      .set('Authorization', `Bearer  ${playerOneToken}`)
      .send(move)
      .expect(httpStatus.NO_CONTENT);
    game = await Game.findById(gameId);
    expect(game.state).to.be.equal(GameState.playerOneWon);
  });

  it('should not modify a finished game', async () => {
    const row = 2;
    const column = 0;
    const move = {
      destination: [1, 1],
      captures: [],
    };
    const game = await Game.findById(gameId);
    game.board = FINISHED_GAME_BOARD_TEST;
    game.state = GameState.playerOneWon;
    await game.save();
    await request(app)
      .post(`/v1/game/move/${row}/${column}`)
      .set('Authorization', `Bearer  ${playerOneToken}`)
      .send(move)
      .expect(httpStatus.INTERNAL_SERVER_ERROR);
  });

  it('should return an error if a invalid row or column is provided', async () => {
    const row = 'error';
    const column = 0;
    const move = {
      destination: [1, 1],
      captures: [],
    };

    await request(app)
      .post(`/v1/game/move/${row}/${column}`)
      .set('Authorization', `Bearer  ${playerOneToken}`)
      .send(move)
      .expect(httpStatus.BAD_REQUEST);
  });

  it('should return an error if row or column is outisde boundries', async () => {
    const row = 8;
    const column = 0;
    const move = {
      destination: [1, 1],
      captures: [],
    };

    await request(app)
      .post(`/v1/game/move/${row}/${column}`)
      .set('Authorization', `Bearer  ${playerOneToken}`)
      .send(move)
      .expect(httpStatus.BAD_REQUEST);
  });
});
