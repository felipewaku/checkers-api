/* eslint-disable arrow-body-style */
/* eslint-disable no-unused-expressions */
const request = require('supertest');
const httpStatus = require('http-status');
const { expect } = require('chai');
const app = require('../../../index');
const { Game } = require('../../models/game.model');
const { GameState } = require('../../models/game.entities');


describe('POST /v1/game', async () => {
  afterEach(async () => {
    await Game.remove({});
  });


  it('should create a new game', async () => {
    const res = await request(app)
      .post('/v1/game')
      .expect(httpStatus.CREATED);
    expect(res.headers).to.have.property('authorization');
    const games = await Game.find();
    expect(games).to.have.lengthOf(1);
    const [createdGame] = games;
    expect(res.body.gameId).to.be.equal(createdGame.id);
    expect(createdGame.state).to.be.equal(GameState.waitingOpponent);
  });
});
