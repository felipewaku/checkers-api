/* eslint-disable arrow-body-style */
/* eslint-disable no-unused-expressions */
const request = require('supertest');
const httpStatus = require('http-status');
const { expect } = require('chai');
const app = require('../../../index');
const { Game } = require('../../models/game.model');

describe('GET /v1/game/:row/:column', async () => {
  let playerOneToken;

  beforeEach(async () => {
    const { gameId, playerOneToken: token } = await Game.createGame();
    playerOneToken = token;
    const game = await Game.findById(gameId);
    await game.setupPlayerTwo();
  });

  afterEach(async () => {
    await Game.remove({});
  });

  it('should return the moves of a piece', async () => {
    const row = 5;
    const column = 0;
    const res = await request(app)
      .get(`/v1/game/move/${row}/${column}`)
      .set('Authorization', `Bearer  ${playerOneToken}`)
      .expect(httpStatus.OK);
    expect(res.body.moves).to.have.lengthOf(1);
  });

  it('should return an error if a invalid row or column is provided', async () => {
    const row = 'error';
    const column = 0;
    await request(app)
      .get(`/v1/game/move/${row}/${column}`)
      .set('Authorization', `Bearer  ${playerOneToken}`)
      .expect(httpStatus.BAD_REQUEST);
  });

  it('should return an error if row or column is outisde boundries', async () => {
    const row = 100;
    const column = 0;
    await request(app)
      .get(`/v1/game/move/${row}/${column}`)
      .set('Authorization', `Bearer  ${playerOneToken}`)
      .expect(httpStatus.BAD_REQUEST);
  });
});
