/* eslint-disable arrow-body-style */
/* eslint-disable no-unused-expressions */
const request = require('supertest');
const httpStatus = require('http-status');
const { expect } = require('chai');
const app = require('../../../index');
const { Game } = require('../../models/game.model');

describe('GET /v1/game', async () => {
  let playerOneToken;

  beforeEach(async () => {
    const { gameId, playerOneToken: token } = await Game.createGame();
    playerOneToken = token;
    const game = await Game.findById(gameId);
    await game.setupPlayerTwo();
  });

  afterEach(async () => {
    await Game.remove({});
  });

  it('should return the board and state of the game', async () => {
    const res = await request(app)
      .get('/v1/game')
      .set('Authorization', `Bearer  ${playerOneToken}`)
      .expect(httpStatus.OK);


    const [game] = await Game.find();
    res.body.board.forEach((row, rowIndex) => {
      row.forEach((tile, columnIndex) => {
        expect(tile).to.be.equal(game.board[rowIndex][columnIndex]);
      });
    });

    expect(game.state).to.be.equal(res.body.state);
  });

  it('should throw an error if do not send an authorization token', async () => {
    await request(app)
      .get('/v1/game')
      .expect(httpStatus.UNAUTHORIZED);
  });
});
