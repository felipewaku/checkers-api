/* eslint-disable arrow-body-style */
/* eslint-disable no-unused-expressions */
const { expect } = require('chai');
const { Piece, PlayerTurn } = require('../../models/game.entities');
const getMovesOfPiece = require('../../services/piece-moves.service');

const MOVEMENT_TEST_BOARD = [
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, Piece.menWhite, null],
  [null, null, null, Piece.kingWhite, null, Piece.kingWhite, null, null],
  [null, null, null, null, null, null, Piece.menWhite, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
];

const CAPTURE_TEST_BOARD = [
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, Piece.menBlack, null, Piece.menBlack, null, null, null],
  [null, null, null, Piece.kingWhite, null, null, null, null],
  [null, null, Piece.menBlack, null, Piece.menBlack, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
];

const MULTIPLE_CAPTURE_TEST_BOARD = [
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, Piece.menBlack, null, Piece.menBlack, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, Piece.menBlack, null, Piece.menBlack, null, null, null],
  [null, null, null, Piece.kingWhite, null, null, null, null],
  [null, null, null, null, null, null, null, null],
];


describe('Piece moves service', () => {
  describe('King movement', () => {
    it('should return four moves if there are no pieces around it for player 1', () => {
      const moves = getMovesOfPiece(MOVEMENT_TEST_BOARD, 4, 3, PlayerTurn.playerOne);
      expect(moves).to.have.lengthOf(4);
      const leftMoveTop = moves.find(move => move.destination[0] === 3 && move.destination[1] === 2);
      expect(leftMoveTop).to.be.an('object');
      expect(leftMoveTop.captures).to.have.lengthOf(0);
      const rightMoveTop = moves.find(move => move.destination[0] === 3 && move.destination[1] === 4);
      expect(rightMoveTop).to.be.an('object');
      expect(rightMoveTop.captures).to.have.lengthOf(0);

      const leftMoveBottom = moves.find(move => move.destination[0] === 5 && move.destination[1] === 2);
      expect(leftMoveBottom).to.be.an('object');
      expect(leftMoveBottom.captures).to.have.lengthOf(0);
      const rightMoveBottom = moves.find(move => move.destination[0] === 5 && move.destination[1] === 4);
      expect(rightMoveBottom).to.be.an('object');
      expect(rightMoveBottom.captures).to.have.lengthOf(0);
    });

    it('should return two moves if there are pieces blockins its path', () => {
      const moves = getMovesOfPiece(MOVEMENT_TEST_BOARD, 4, 5, PlayerTurn.playerOne);
      expect(moves).to.have.lengthOf(2);
      const leftMoveTop = moves.find(move => move.destination[0] === 3 && move.destination[1] === 4);
      expect(leftMoveTop).to.be.an('object');
      expect(leftMoveTop.captures).to.have.lengthOf(0);

      const leftMoveBottom = moves.find(move => move.destination[0] === 5 && move.destination[1] === 4);
      expect(leftMoveBottom).to.be.an('object');
      expect(leftMoveBottom.captures).to.have.lengthOf(0);
    });
  });

  describe('King capture', () => {
    it('should return captures on all sides', () => {
      const moves = getMovesOfPiece(CAPTURE_TEST_BOARD, 4, 3, PlayerTurn.playerOne);
      expect(moves).to.have.lengthOf(4);
      const leftMoveTop = moves.find(move => move.destination[0] === 2 && move.destination[1] === 1);
      expect(leftMoveTop).to.be.an('object');
      expect(leftMoveTop.captures).to.have.lengthOf(1);
      const rightMoveTop = moves.find(move => move.destination[0] === 2 && move.destination[1] === 5);
      expect(rightMoveTop).to.be.an('object');
      expect(rightMoveTop.captures).to.have.lengthOf(1);

      const leftMoveBottom = moves.find(move => move.destination[0] === 6 && move.destination[1] === 1);
      expect(leftMoveBottom).to.be.an('object');
      expect(leftMoveBottom.captures).to.have.lengthOf(1);
      const rightMoveBottom = moves.find(move => move.destination[0] === 6 && move.destination[1] === 5);
      expect(rightMoveBottom).to.be.an('object');
      expect(rightMoveBottom.captures).to.have.lengthOf(1);
    });

    it('should handle multiple captures on all sides', () => {
      const moves = getMovesOfPiece(MULTIPLE_CAPTURE_TEST_BOARD, 6, 3, PlayerTurn.playerOne);
      expect(moves).to.have.lengthOf(2);
      const firstMove = moves[0];
      expect(firstMove).to.be.an('object');
      expect(firstMove.captures).to.have.lengthOf(4);
    });
  });
});
