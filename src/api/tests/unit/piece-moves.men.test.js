/* eslint-disable arrow-body-style */
/* eslint-disable no-unused-expressions */
const { expect } = require('chai');
const { Piece, PlayerTurn } = require('../../models/game.entities');
const getMovesOfPiece = require('../../services/piece-moves.service');

const MOVEMENT_TEST_BOARD = [
  [null, Piece.menBlack, null, null, null, Piece.menBlack, null, Piece.menBlack],
  [Piece.menBlack, null, Piece.menBlack, null, Piece.menBlack, null, Piece.menBlack, null],
  [null, Piece.menBlack, null, Piece.menBlack, null, Piece.menBlack, null, Piece.menBlack],
  [null, null, null, null, Piece.menBlack, null, null, null],
  [null, null, null, null, null, null, null, Piece.menWhite],
  [Piece.menWhite, null, Piece.menWhite, null, Piece.menWhite, null, Piece.menWhite, null],
  [null, Piece.menWhite, null, Piece.menWhite, null, Piece.menWhite, null, Piece.menWhite],
  [Piece.menWhite, null, null, null, Piece.menWhite, null, Piece.menWhite, null],
];

const CAPTURE_TEST_BOARD = [
  [null, Piece.menBlack, null, Piece.menBlack, null, null, null, null],
  [Piece.menWhite, null, Piece.menWhite, null, Piece.menWhite, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, Piece.menBlack, null, Piece.menBlack, null, null],
  [null, null, null, null, Piece.menWhite, null, null, null],
  [null, Piece.menBlack, null, null, null, null, null, null],
  [null, null, Piece.menWhite, null, null, null, null, null],
];

const MULTIPLE_CAPTURES_TEST_BOARD = [
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, Piece.menBlack, null, Piece.menBlack, null, Piece.menBlack, null, null],
  [null, null, null, null, null, null, null, null],
  [null, Piece.menBlack, null, null, null, Piece.menBlack, null, null],
  [null, null, Piece.menWhite, null, null, null, Piece.menWhite, null],
];

const THREE_MULTIPLE_CAPTURES_TEST_BOARD = [
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, Piece.menBlack, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, Piece.menBlack, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, Piece.menBlack, null, null],
  [null, null, null, null, null, null, Piece.menWhite, null],
];

const MULTIPLE_CAPTURES_WITH_MORE_DESTINATIONS_TEST_BOARD = [
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, Piece.menBlack, null, Piece.menBlack, null, Piece.menBlack, null, null],
  [null, null, null, null, null, null, null, null],
  [null, Piece.menBlack, null, Piece.menBlack, null, Piece.menBlack, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, Piece.menBlack, null, Piece.menBlack, null, null],
  [null, null, null, null, Piece.menWhite, null, null, null],
];

const MULTIPLE_CAPTURES_WITH_MORE_DESTINATIONS_TEST_BOARD_2 = [
  [null, null, null, Piece.menBlack, null, null, null, null],
  [null, null, Piece.menWhite, null, Piece.menWhite, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, Piece.menWhite, null, Piece.menWhite, null, Piece.menWhite, null],
  [null, null, null, null, null, null, null, null],
  [null, null, Piece.menWhite, null, Piece.menWhite, null, Piece.menWhite, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
];

describe('Piece moves service', () => {
  describe('Exceptions', () => {
    it('should throw an error if tries to check the movement of a non belong piece', () => {
      const fn = function fn() { getMovesOfPiece(MOVEMENT_TEST_BOARD, 5, 0, PlayerTurn.playerTwo); };
      expect(fn).to.throw();
    });

    it('should throw an error if tries to check the movement of a non existing piece', () => {
      const fn = function fn() { getMovesOfPiece(MOVEMENT_TEST_BOARD, 0, 0, PlayerTurn.playerTwo); };
      expect(fn).to.throw();
    });
  });

  describe('Men movement', () => {
    it('should return two moves if there are no pieces ahead for player 1', () => {
      const moves = getMovesOfPiece(MOVEMENT_TEST_BOARD, 5, 2, PlayerTurn.playerOne);
      expect(moves).to.have.lengthOf(2);
      const leftMove = moves.find(move => move.destination[0] === 4 && move.destination[1] === 1);
      expect(leftMove).to.be.an('object');
      expect(leftMove.captures).to.have.lengthOf(0);
      const rightMove = moves.find(move => move.destination[0] === 4 && move.destination[1] === 3);
      expect(rightMove).to.be.an('object');
      expect(rightMove.captures).to.have.lengthOf(0);
    });

    it('should return two moves if there are no pieces ahead for player 2', () => {
      const moves = getMovesOfPiece(MOVEMENT_TEST_BOARD, 2, 1, PlayerTurn.playerTwo);
      expect(moves).to.have.lengthOf(2);
      const leftMove = moves.find(move => move.destination[0] === 3 && move.destination[1] === 0);
      expect(leftMove).to.be.an('object');
      expect(leftMove.captures).to.have.lengthOf(0);
      const rightMove = moves.find(move => move.destination[0] === 3 && move.destination[1] === 2);
      expect(rightMove).to.be.an('object');
      expect(rightMove.captures).to.have.lengthOf(0);
    });

    it('should return one move if the piece is in the border for player 1', () => {
      const moves = getMovesOfPiece(MOVEMENT_TEST_BOARD, 5, 0, PlayerTurn.playerOne);
      expect(moves).to.have.lengthOf(1);
      const rightMove = moves.find(move => move.destination[0] === 4 && move.destination[1] === 1);
      expect(rightMove).to.be.an('object');
      expect(rightMove.captures).to.have.lengthOf(0);
    });

    it('should return one move if the piece is in the border for player 2', () => {
      const moves = getMovesOfPiece(MOVEMENT_TEST_BOARD, 2, 7, PlayerTurn.playerTwo);
      expect(moves).to.have.lengthOf(1);
      const leftMove = moves.find(move => move.destination[0] === 3 && move.destination[1] === 6);
      expect(leftMove).to.be.an('object');
      expect(leftMove.captures).to.have.lengthOf(0);
    });

    it('should return one move if there is a friendly piece blocking a possible move for player 1', () => {
      const moves = getMovesOfPiece(MOVEMENT_TEST_BOARD, 5, 6, PlayerTurn.playerOne);
      expect(moves).to.have.lengthOf(1);
      const leftMove = moves.find(move => move.destination[0] === 4 && move.destination[1] === 5);
      expect(leftMove).to.be.an('object');
      expect(leftMove.captures).to.have.lengthOf(0);
    });

    it('should return one move if there is a friendly piece blocking a possible move for player 2', () => {
      const moves = getMovesOfPiece(MOVEMENT_TEST_BOARD, 2, 3, PlayerTurn.playerTwo);
      expect(moves).to.have.lengthOf(1);
      const leftMove = moves.find(move => move.destination[0] === 3 && move.destination[1] === 2);
      expect(leftMove).to.be.an('object');
      expect(leftMove.captures).to.have.lengthOf(0);
    });
  });

  describe('Men capure', () => {
    it('should return two captures if possible', () => {
      const moves = getMovesOfPiece(CAPTURE_TEST_BOARD, 5, 4, PlayerTurn.playerOne);
      expect(moves).to.have.lengthOf(2);
      const leftMove = moves.find(move => move.destination[0] === 3 && move.destination[1] === 2);
      expect(leftMove).to.be.an('object');
      expect(leftMove.captures).to.have.lengthOf(1);
      expect(leftMove.captures[0][0]).to.be.equal(4);
      expect(leftMove.captures[0][1]).to.be.equal(3);

      const right = moves.find(move => move.destination[0] === 3 && move.destination[1] === 6);
      expect(right).to.be.an('object');
      expect(right.captures).to.have.lengthOf(1);
      expect(right.captures[0][0]).to.be.equal(4);
      expect(right.captures[0][1]).to.be.equal(5);
    });

    it('should return one capture if there is a possible capture', () => {
      const moves = getMovesOfPiece(CAPTURE_TEST_BOARD, 7, 2, PlayerTurn.playerOne);
      expect(moves).to.have.lengthOf(1);
      const leftMove = moves.find(move => move.destination[0] === 5 && move.destination[1] === 0);
      expect(leftMove).to.be.an('object');
      expect(leftMove.captures).to.have.lengthOf(1);
      expect(leftMove.captures[0][0]).to.be.equal(6);
      expect(leftMove.captures[0][1]).to.be.equal(1);
    });

    it('should return one capture if there is a possible capture for player 2', () => {
      const moves = getMovesOfPiece(CAPTURE_TEST_BOARD, 0, 1, PlayerTurn.playerTwo);
      expect(moves).to.have.lengthOf(1);
      const leftMove = moves.find(move => move.destination[0] === 2 && move.destination[1] === 3);
      expect(leftMove).to.be.an('object');
      expect(leftMove.captures).to.have.lengthOf(1);
      expect(leftMove.captures[0][0]).to.be.equal(1);
      expect(leftMove.captures[0][1]).to.be.equal(2);
    });

    it('should return two captures if possible for player 2', () => {
      const moves = getMovesOfPiece(CAPTURE_TEST_BOARD, 0, 3, PlayerTurn.playerTwo);
      expect(moves).to.have.lengthOf(2);
      const leftMove = moves.find(move => move.destination[0] === 2 && move.destination[1] === 1);
      expect(leftMove).to.be.an('object');
      expect(leftMove.captures).to.have.lengthOf(1);
      expect(leftMove.captures[0][0]).to.be.equal(1);
      expect(leftMove.captures[0][1]).to.be.equal(2);

      const right = moves.find(move => move.destination[0] === 2 && move.destination[1] === 5);
      expect(right).to.be.an('object');
      expect(right.captures).to.have.lengthOf(1);
      expect(right.captures[0][0]).to.be.equal(1);
      expect(right.captures[0][1]).to.be.equal(4);
    });

    it('should return two captures', () => {
      const moves = getMovesOfPiece(MULTIPLE_CAPTURES_TEST_BOARD, 7, 2, PlayerTurn.playerOne);
      expect(moves).to.have.lengthOf(1);
      const leftMove = moves.find(move => move.destination[0] === 3 && move.destination[1] === 2);
      expect(leftMove).to.be.an('object');
      expect(leftMove.captures).to.have.lengthOf(2);
      expect(leftMove.captures[0][0]).to.be.equal(4);
      expect(leftMove.captures[0][1]).to.be.equal(1);
      expect(leftMove.captures[1][0]).to.be.equal(6);
      expect(leftMove.captures[1][1]).to.be.equal(1);
    });

    it('should return two captures with two possible destinations', () => {
      const moves = getMovesOfPiece(MULTIPLE_CAPTURES_TEST_BOARD, 7, 6, PlayerTurn.playerOne);
      expect(moves).to.have.lengthOf(2);
      let move = moves.find(item => item.destination[0] === 3 && item.destination[1] === 2);
      expect(move).to.be.an('object');
      expect(move.captures).to.have.lengthOf(2);
      expect(move.captures[0][0]).to.be.equal(4);
      expect(move.captures[0][1]).to.be.equal(3);
      expect(move.captures[1][0]).to.be.equal(6);
      expect(move.captures[1][1]).to.be.equal(5);

      move = moves.find(item => item.destination[0] === 3 && item.destination[1] === 6);
      expect(move).to.be.an('object');
      expect(move.captures).to.have.lengthOf(2);
      expect(move.captures[0][0]).to.be.equal(4);
      expect(move.captures[0][1]).to.be.equal(5);
      expect(move.captures[1][0]).to.be.equal(6);
      expect(move.captures[1][1]).to.be.equal(5);
    });

    it('should return three captures', () => {
      const moves = getMovesOfPiece(THREE_MULTIPLE_CAPTURES_TEST_BOARD, 7, 6, PlayerTurn.playerOne);
      expect(moves).to.have.lengthOf(1);
      const move = moves.find(item => item.destination[0] === 1 && item.destination[1] === 4);
      expect(move).to.be.an('object');
      expect(move.captures).to.have.lengthOf(3);
      expect(move.captures[0][0]).to.be.equal(2);
      expect(move.captures[0][1]).to.be.equal(5);
      expect(move.captures[1][0]).to.be.equal(4);
      expect(move.captures[1][1]).to.be.equal(5);
      expect(move.captures[2][0]).to.be.equal(6);
      expect(move.captures[2][1]).to.be.equal(5);
    });


    it('should return multiple destinations', () => {
      const moves = getMovesOfPiece(MULTIPLE_CAPTURES_TEST_BOARD, 7, 6, PlayerTurn.playerOne);
      expect(moves).to.have.lengthOf(2);
      let move = moves.find(item => item.destination[0] === 3 && item.destination[1] === 2);
      expect(move).to.be.an('object');
      expect(move.captures).to.have.lengthOf(2);
      expect(move.captures[0][0]).to.be.equal(4);
      expect(move.captures[0][1]).to.be.equal(3);
      expect(move.captures[1][0]).to.be.equal(6);
      expect(move.captures[1][1]).to.be.equal(5);

      move = moves.find(item => item.destination[0] === 3 && item.destination[1] === 6);
      expect(move).to.be.an('object');
      expect(move.captures).to.have.lengthOf(2);
      expect(move.captures[0][0]).to.be.equal(4);
      expect(move.captures[0][1]).to.be.equal(5);
      expect(move.captures[1][0]).to.be.equal(6);
      expect(move.captures[1][1]).to.be.equal(5);
    });

    it('should return multiple destinations and captures', () => {
      const moves = getMovesOfPiece(MULTIPLE_CAPTURES_WITH_MORE_DESTINATIONS_TEST_BOARD, 7, 4, PlayerTurn.playerOne);
      expect(moves).to.have.lengthOf(5);
    });

    it('should return multiple destinations and captures for player 2', () => {
      const moves = getMovesOfPiece(MULTIPLE_CAPTURES_WITH_MORE_DESTINATIONS_TEST_BOARD_2, 0, 3, PlayerTurn.playerTwo);
      expect(moves).to.have.lengthOf(5);
    });
  });
});
