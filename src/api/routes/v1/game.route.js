const express = require('express');
const controller = require('../../controllers/game.controller');
const { authorize } = require('../../middlewares/authorize');
const { sanitize } = require('../../middlewares/sanitize');

const router = express.Router();

router.param('gameId', controller.load);

/**
 * @api {post} v1/game Create new Game
 * @apiDescription Create a new game
 * @apiVersion 1.0.0
 * @apiName Create new
 * @apiGroup Game
 * @apiPermission public
 *
 * @apiSuccess (Created 201) {String}  token.playerTwoToken   Player two's  id
 * @apiSuccess (Created 201) {String}  gameId                 The game's id
 *
 */
router.route('/').post(controller.create);

/**
 * @api {post} v1/game/:gameId/register Register player two to a game
 * @apiDescription Register the second player to a game
 * @apiVersion 1.0.0
 * @apiName Register Player Two
 * @apiGroup Game
 * @apiPermission public
 *
 * @apiSuccess (OK 200) {String}  gameId                 The game's id
 *
 */
router.route('/:gameId/register').post(controller.registerPlayerTwo);

/**
 * @api {post} v1/game Get the game's current state
 * @apiDescription Return the game's current state (Board and Game state)
 * @apiVersion 1.0.0
 * @apiName Get State
 * @apiGroup Game
 * @apiPermission authenticated
 *
 * @apiSuccess (OK 200) {String}  board The current game's board
 * @apiSuccess (OK 200) {String}  state The current game's state
 *
 */
router.route('/').get(authorize, controller.get);

/**
 * @api {get} v1/game/moves/:index Moves of piece
 * @apiDescription Return the piece's possible movements
 * @apiVersion 1.0.0
 * @apiName Get moves
 * @apiGroup Game
 * @apiPermission authenticated
 *
 */
router.route('/move/:row/:column').get(authorize, sanitize, controller.getMoves);


/**
 * @api {post} v1/game/moves/:index Moves of piece
 * @apiDescription Move the piece to a selected position
 * @apiVersion 1.0.0
 * @apiName Get moves
 * @apiGroup Game
 * @apiPermission authenticated
 */
router.route('/move/:row/:column').post(authorize, sanitize, controller.move);

module.exports = router;
