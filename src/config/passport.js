const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const { jwtSecret } = require('./vars');
const { Game } = require('../api/models/game.model');
const { PlayerTurn } = require('../api/models/game.entities');

const jwtOptions = {
  secretOrKey: jwtSecret,
  jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Bearer'),
};

const gameJwt = async (payload, done) => {
  try {
    const tokenData = {};
    tokenData.game = await Game.findById(payload.gameId);
    const isPlayerOneRequest =
      payload.playerOne === tokenData.game.playerOne.toString();
    const isPlayerTwoRequest =
      tokenData.game.playerTwo &&
      payload.playerTwo === tokenData.game.playerTwo.toString();
    if (isPlayerOneRequest) {
      tokenData.requestPlayer = PlayerTurn.playerOne;
    } else if (isPlayerTwoRequest) {
      tokenData.requestPlayer = PlayerTurn.playerTwo;
    }
    if (tokenData.game && tokenData.requestPlayer) {
      return done(null, tokenData);
    }
    return done(null, false);
  } catch (error) {
    return done(error, false);
  }
};

exports.gameJwt = new JwtStrategy(jwtOptions, gameJwt);
